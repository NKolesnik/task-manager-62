package ru.t1consulting.nkolesnik.tm.dto.response.project;

import lombok.NoArgsConstructor;
import ru.t1consulting.nkolesnik.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public class ProjectClearResponse extends AbstractResponse {

}
