package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.listener.AbstractListener;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractListener command);

    @NotNull
    Collection<AbstractListener> getTerminalCommands();

    @Nullable
    AbstractListener getCommandByName(@Nullable String name);

    @Nullable
    AbstractListener getCommandByArgument(@Nullable String argument);

    @NotNull
    Iterable<AbstractListener> getCommandsWithArguments();

}
