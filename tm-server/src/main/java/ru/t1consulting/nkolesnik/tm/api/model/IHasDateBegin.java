package ru.t1consulting.nkolesnik.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasDateBegin {

    @Nullable
    Date getDateBegin();

    void setDateBegin(@NotNull Date dateBegin);

}
