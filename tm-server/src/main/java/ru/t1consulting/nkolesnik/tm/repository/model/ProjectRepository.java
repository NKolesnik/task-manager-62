package ru.t1consulting.nkolesnik.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.List;

@Repository
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    @Nullable
    @Query("SELECT p FROM Project p WHERE p.id=:id AND p.user.id=:userId")
    Project findById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @NotNull
    @Query("SELECT p FROM Project p WHERE p.user.id=:userId")
    List<Project> findAll(
            @Nullable @Param("userId") final String userId
    );

    @NotNull
    @Query("SELECT p FROM Project p WHERE p.user.id=:userId ORDER BY :sortOrder")
    List<Project> findAll(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("sortOrder") final String sortOrder
    );

    @Query("SELECT COUNT(1) = 1 FROM Project p WHERE p.id = :id AND p.user.id = :userId")
    boolean existsById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @Query("SELECT COUNT(p) FROM Project p WHERE p.user.id=:userId")
    long getSize(
            @Nullable @Param("userId") final String userId
    );

    @Modifying
    @Query("DELETE FROM Project p WHERE p.user.id=:userId AND p.id=:id")
    void removeById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id
    );

    @Modifying
    @Query("DELETE FROM Project p WHERE p.user.id=:userId")
    void clear(
            @Nullable @Param("userId") final String userId
    );

    @Modifying
    @Query("UPDATE Project p SET p.status=:status WHERE p.id=:id AND p.user.id=:userId")
    void changeProjectStatusById(
            @Nullable @Param("userId") final String userId,
            @Nullable @Param("id") final String id,
            @Nullable @Param("status") final String status
    );

}
