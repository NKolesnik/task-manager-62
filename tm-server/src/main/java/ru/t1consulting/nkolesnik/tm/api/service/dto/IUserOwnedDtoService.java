package ru.t1consulting.nkolesnik.tm.api.service.dto;

import ru.t1consulting.nkolesnik.tm.dto.model.AbstractUserOwnedModelDto;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedModelDto> extends IDtoService<M> {

}
