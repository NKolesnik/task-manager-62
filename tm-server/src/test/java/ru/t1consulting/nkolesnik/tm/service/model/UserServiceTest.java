package ru.t1consulting.nkolesnik.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IProjectService;
import ru.t1consulting.nkolesnik.tm.api.service.model.ITaskService;
import ru.t1consulting.nkolesnik.tm.api.service.model.IUserService;
import ru.t1consulting.nkolesnik.tm.config.ServerConfiguration;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.service.PropertyService;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.util.List;
import java.util.UUID;
import java.util.Vector;

public class UserServiceTest {

    @NotNull
    private static final String USER_LOGIN_PREFIX = "TEST_USER_LOGIN";

    @NotNull
    private static final String USER_EMAIL_PREFIX = "TEST_USER_@EMAIL";

    @NotNull
    private static final String USER_PASSWORD_PREFIX = "TEST_USER_PASSWORD";

    @NotNull
    private static final String USER_PASSWORD_SECRET = "123654789";

    @NotNull
    private static final Integer USER_PASSWORD_ITERATION = 3;

    @NotNull
    private static final String USER_FIRST_NAME = "TEST";

    @NotNull
    private static final String USER_MIDDLE_NAME = "TEST";

    @NotNull
    private static final String USER_LAST_NAME = "TEST";

    private static final long REPOSITORY_SIZE = 100L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @Nullable
    private static final User NULL_USER = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String EMPTY_USER_ID = null;

    @Nullable
    private static final String NULL_USER_EMAIL = null;

    @Nullable
    private static final String NULL_USER_LOGIN = null;

    @Nullable
    private static final Role NULL_USER_ROLE = null;

    @Nullable
    private static final String NULL_USER_PASSWORD = null;

    @NotNull
    private static final String EMPTY_USER_LOGIN = "";

    @NotNull
    private static final String EMPTY_USER_PASSWORD = "";

    @NotNull
    private static final String EMPTY_USER_EMAIL = "";

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "PROJECT_TASK_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "PROJECT_TASK_DESCRIPTION";

    @NotNull
    private static final String USER_ID = UUID.randomUUID().toString();

    @NotNull
    private static final String PROJECT_ID = UUID.randomUUID().toString();

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private IUserService userService;

    @NotNull
    private ITaskService taskService;
    @NotNull
    private User user;

    @NotNull
    private Project project;

    @NotNull
    private List<Task> tasks;

    @Before
    public void setup() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
                ServerConfiguration.class
        );

        projectService = context.getBean(IProjectService.class);
        propertyService = context.getBean(PropertyService.class);
        userService = context.getBean(IUserService.class);
        taskService = context.getBean(ITaskService.class);

        user = createUser();
        tasks = createManyTasks();
        project = createOneProject();
    }

    @After
    public void cleanup() {
        taskService.clear();
        projectService.clear();
        userService.clear();
    }

    @Test
    public void create() {
        userService.add(user);
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create(EMPTY_USER_LOGIN, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create(NULL_USER_LOGIN, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", EMPTY_USER_PASSWORD)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", NULL_USER_PASSWORD)
        );
        Assert.assertThrows(
                LoginAlreadyExistException.class,
                () -> userService.create(USER_LOGIN_PREFIX, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                LoginAlreadyExistException.class,
                () -> userService.create(USER_LOGIN_PREFIX, USER_PASSWORD_PREFIX, USER_EMAIL_PREFIX + "TEST")
        );
        Assert.assertThrows(
                RoleIsEmptyException.class,
                () -> userService.create(
                        USER_LOGIN_PREFIX + "TEST",
                        USER_PASSWORD_PREFIX,
                        EMPTY_USER_EMAIL + "TEST",
                        NULL_USER_ROLE
                )
        );
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", USER_PASSWORD_PREFIX, EMPTY_USER_EMAIL)
        );
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", USER_PASSWORD_PREFIX, NULL_USER_EMAIL)
        );
        Assert.assertThrows(
                EmailAlreadyExistException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", USER_PASSWORD_PREFIX, USER_EMAIL_PREFIX)
        );
        userService.create(USER_LOGIN_PREFIX + "TEST", USER_PASSWORD_PREFIX + "TEST");
        userService.create(USER_LOGIN_PREFIX + "TEST1", USER_PASSWORD_PREFIX + "TEST", Role.USUAL);
        userService.create(
                USER_LOGIN_PREFIX + "TEST2",
                USER_PASSWORD_PREFIX + "TEST",
                USER_EMAIL_PREFIX + "TEST2"
        );
        userService.create(
                USER_LOGIN_PREFIX + "TEST3",
                USER_PASSWORD_PREFIX + "TEST",
                USER_EMAIL_PREFIX + "TEST3",
                Role.USUAL
        );
        Assert.assertEquals(5, userService.getSize());
    }

    @Test
    public void findByLogin() {
        userService.add(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(NULL_USER_LOGIN));
        Assert.assertNull(userService.findByLogin(UUID.randomUUID().toString()));
        @Nullable final User repositoryUser = userService.findByLogin(USER_LOGIN_PREFIX);
        Assert.assertNotNull(repositoryUser);
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), repositoryUser.getId());
    }

    @Test
    public void findByEmail() {
        userService.add(user);
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(EMPTY_USER_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(NULL_USER_EMAIL));
        Assert.assertNull(userService.findByEmail(UUID.randomUUID().toString()));
        @Nullable final User repositoryUser = userService.findByEmail(USER_EMAIL_PREFIX);
        Assert.assertNotNull(user);
        Assert.assertNotNull(repositoryUser);
        Assert.assertEquals(user.getId(), repositoryUser.getId());
    }

    @Test
    public void setPassword() {
        userService.add(user);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.setPassword(NULL_USER_ID, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.setPassword(EMPTY_USER_ID, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(USER_ID, EMPTY_USER_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(USER_ID, NULL_USER_PASSWORD));
        Assert.assertNotNull(user);
        @Nullable final String oldPassword = user.getPasswordHash();
        userService.setPassword(USER_ID, "NEW PASSWORD");
        @Nullable User repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertNotEquals(oldPassword, repositoryUser.getPasswordHash());
    }

    @Test
    public void updateUser() {
        userService.add(user);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.updateUser(NULL_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.updateUser(EMPTY_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME)
        );
        userService.updateUser(USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME);
        Assert.assertNotNull(user);
        @Nullable User repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertEquals(repositoryUser.getFirstName(), USER_FIRST_NAME);
        Assert.assertEquals(repositoryUser.getMiddleName(), USER_MIDDLE_NAME);
        Assert.assertEquals(repositoryUser.getLastName(), USER_LAST_NAME);
    }

    @Test
    public void remove() {
        userService.add(user);
        projectService.add(project);
        taskService.add(tasks);
        Assert.assertThrows(UserNotFoundException.class, () -> userService.remove(NULL_USER));
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize(USER_ID));
        Assert.assertEquals(1, projectService.getSize(USER_ID));
        Assert.assertEquals(1, userService.getSize());
        userService.remove(user);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize(USER_ID));
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize(USER_ID));
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, userService.getSize());
    }

    @Test
    public void removeByLogin() {
        userService.add(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin(UUID.randomUUID().toString()));
        Assert.assertNotNull(user);
        userService.removeByLogin(user.getLogin());
        @Nullable final User repositoryUser = userService.findById(USER_ID);
        Assert.assertNull(repositoryUser);
    }

    @Test
    public void isLoginExist() {
        userService.add(user);
        Assert.assertFalse(userService.isLoginExist(EMPTY_USER_LOGIN));
        Assert.assertFalse(userService.isLoginExist(NULL_USER_LOGIN));
        Assert.assertFalse(userService.isLoginExist(UUID.randomUUID().toString()));
        Assert.assertTrue(userService.isLoginExist(USER_LOGIN_PREFIX));
    }

    @Test
    public void isEmailExist() {
        userService.add(user);
        Assert.assertFalse(userService.isEmailExist(EMPTY_USER_EMAIL));
        Assert.assertFalse(userService.isEmailExist(NULL_USER_EMAIL));
        Assert.assertFalse(userService.isEmailExist(UUID.randomUUID().toString()));
        Assert.assertTrue(userService.isEmailExist(USER_EMAIL_PREFIX));
    }

    @Test
    public void lockUserByLogin() {
        userService.add(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.lockUserByLogin(UUID.randomUUID().toString())
        );
        userService.lockUserByLogin(USER_LOGIN_PREFIX);
        Assert.assertNotNull(user);
        @Nullable final User repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertTrue(repositoryUser.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        userService.add(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.unlockUserByLogin(UUID.randomUUID().toString())
        );
        userService.lockUserByLogin(USER_LOGIN_PREFIX);
        Assert.assertNotNull(user);
        @Nullable User repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertTrue(repositoryUser.getLocked());
        userService.unlockUserByLogin(USER_LOGIN_PREFIX);
        repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertFalse(repositoryUser.getLocked());
    }

    @NotNull
    private User createUser() {
        @NotNull final User user = new User();
        user.setId(USER_ID);
        user.setLogin(USER_LOGIN_PREFIX);
        user.setPasswordHash(HashUtil.salt(USER_PASSWORD_PREFIX, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        return user;
    }

    @NotNull
    private List<Task> createManyTasks() {
        @NotNull final List<Task> tasks = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Task task = new Task();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUser(user);
            task.setProject(project);
            tasks.add(task);
        }
        return tasks;
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(PROJECT_ID);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        project.setUser(user);
        return project;
    }

}
